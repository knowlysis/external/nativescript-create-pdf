import * as app from 'tns-core-modules/application';
import * as fs from 'tns-core-modules/file-system';
import {ImageSource} from 'tns-core-modules/image-source';
import { ImageAsset } from 'tns-core-modules/image-asset/image-asset';
import { knownFolders, Folder } from "tns-core-modules/file-system";

global['window'] = {
 'document': {
 'createElementNS': () => { return {}; }
}
};
global['document'] = {
 'createElement': (str) => { return {}; }
};
global['navigator'] = {};
const base64 = require('base-64');
const jsPDF = require('./node_modules/jspdf');
global['btoa'] = (str) => {
 return base64.encode(str);
};
global['atob'] = (bytes) => {
 return base64.decode(bytes);
};
global['u8'] = {};

declare var android;
let doc;

export interface PDFType {
    written: boolean;
    message: string;
}

export class CreatePdf {
    exportPath: fs.Folder;
    constructor() {}

    /*
     * Create a PDF file with the given settings, text, and images
     * @param fileSettings Settings for writing pdf to file.
     * @param textArray Array of text, position, fonts for placement in PDF
     * @param images Array of images and their placement in the PDF
     * @returns Promise<PDFType> Indicating if the file was written or failed with a message.
     */
    async create(fileSettings, textArray, images = null): Promise<PDFType> {
        try {
            let externPath = android.os.Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_DOWNLOADS).toString();
            let customFolderPath = fs.path.join(externPath, fileSettings.folderName);
            this.exportPath = fs.Folder.fromPath(customFolderPath);

            doc = new jsPDF(fileSettings.orientation, fileSettings.units, fileSettings.paperSize);
            let pageNum = 1;
            let imageItems = images !== null ? images.filter(x => x.pageNum === pageNum) : [];
            let textItems = textArray.filter(x => x.pageNum === pageNum);
            while (imageItems.length > 0 || textItems.length > 0) {
                if (pageNum > 1) {
                    doc.addPage();
                }
                for (let x of textItems) {
                    if (x.fontColor !== undefined && x.fontColor !== null) {
                        doc.setTextColor(x.fontColor[0], x.fontColor[1], x.fontColor[2]);
                    } else {
                        doc.setTextColor(0, 0, 0);
                    }
                    if (x.fontSize) {
                        doc.setFontSize(x.fontSize);
                    }
                    doc.text(x.data, x.xPos, x.yPos);
                }

                const imgSrc = new ImageSource();
                if (imageItems.length > 0) {
                    for (let img of imageItems) {
                        const fle = knownFolders.currentApp().getFile(img.path);
                        const imageAsset = new ImageAsset(fle.path);

                        await imgSrc.fromAsset(imageAsset);
                        let imgString = await imgSrc.toBase64String('jpeg');
                        imgString = 'data:image/jpeg;base64,' + imgString;

                        doc.addImage(imgString, img.type, img.xPos, img.yPos, img.width, img.height, img.alias, img.compression);
                    }
                }
                pageNum += 1;
                imageItems = images !== null ? images.filter(x => x.pageNum === pageNum) : [];
                textItems = textArray.filter(x => x.pageNum === pageNum);
            }
            // output the entire pdf file to data uri string
            const fileData = doc.output('datauristring');
            // init the file and path to it
            const file: fs.File = <fs.File>this.exportPath.getFile(fileSettings.fileName);
            // Cut out the data piece .. remove 'data:...;base64,'
            const tempData = fileData.split(",")[1];
            // decode the base64 piece
            const data = android.util.Base64.decode(tempData, android.util.Base64.DEFAULT);
            // write to the file
            file.writeSync(data);
            return { written: true, message: `File written.` };
        } catch (e) {
            return { written: false, message: e.message };
        }
    }

}
