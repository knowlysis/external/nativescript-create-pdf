import { knownFolders, File, Folder } from "tns-core-modules/file-system";
import {ImageSource} from 'tns-core-modules/image-source';
import { ImageAsset } from 'tns-core-modules/image-asset/image-asset';
import * as fs from "tns-core-modules/file-system";
import * as utils from "tns-core-modules/utils/utils";

global['window'] = {
 'document': {
 'createElementNS': () => { return {} }
}
};
global['document'] = {
 'createElement': (str) => { return {} }
};
global['navigator'] = {};

const jsPDF = require('./node_modules/jspdf');
const base64 = require('base-64');

global['btoa'] = (str) => {
 return base64.encode(str);
};
global['atob'] = (bytes) => {
 return base64.decode(bytes);
};
global['u8'] = {};

let doc;

export interface PDFType {
    written: boolean;
    message: string;
}
export class CreatePdf {
    folder: any;
    folderName: any;
    fileName: any;
    exportPath: fs.Folder;

    async create(fileSettings, textArray, images = null): Promise<PDFType> {
        try {
            let documents = fs.knownFolders.documents().path;
            // this.folder = documents.getFolder(fileSettings.folderName);
            this.folder = fs.path.join(documents, fileSettings.folderName);
            // this.file = this.folder.getFile(fileSettings.fileName);
            this.exportPath = fs.Folder.fromPath(this.folder);

            doc = new jsPDF(fileSettings.orientation, fileSettings.units, fileSettings.paperSize);
            let pageNum = 1;
            let imageItems = images !== null ? images.filter(x => x.pageNum === pageNum) : [];
            let textItems = textArray.filter(x => x.pageNum === pageNum);
            while (imageItems.length > 0 || textItems.length > 0) {
                if (pageNum > 1) {
                    doc.addPage();
                }
                for (let x of textItems) {
                    if (x.fontColor !== undefined && x.fontColor !== null) {
                        doc.setTextColor(x.fontColor[0], x.fontColor[1], x.fontColor[2]);
                    } else {
                        doc.setTextColor(0, 0, 0);
                    }
                    if (x.fontSize) {
                        doc.setFontSize(x.fontSize);
                    }
                    doc.text(x.data, x.xPos, x.yPos);
                }

                const imgSrc = new ImageSource();
                if (imageItems != null && imageItems.length > 0) {
                    let _base64;
                    for (let img of imageItems) {
                        const fle = knownFolders.currentApp().getFile(img.path);
                        const imageAsset = new ImageAsset(fle.path);
                        // convert image to base64 string
                        await imgSrc.fromAsset(imageAsset).then(res => {
                            let myImgSrc = res;
                            _base64 = myImgSrc.toBase64String('jpeg');
                        });
                        // add data type to base64 string
                        let imgString = 'data:image/jpeg;base64,' + _base64;
                        // add the image to pdf
                        doc.addImage(imgString, img.type, img.xPos, img.yPos, img.width, img.height, img.alias, img.compression);
                    }
                }
                pageNum += 1;
                imageItems = images !== null ? images.filter(x => x.pageNum === pageNum) : [];
                textItems = textArray.filter(x => x.pageNum === pageNum);
            }
            // output the entire pdf file to data uri string
            const fileData = doc.output('datauristring');
            // init the file and path to it
            const file: fs.File = <fs.File>this.exportPath.getFile(fileSettings.fileName);
            // Cut out the data piece .. remove 'data:...;base64,'
            const tempData = fileData.split(",")[1];
            // decode the base64 piece
            // const data = android.util.Base64.decode(tempData, android.util.Base64.DEFAULT);
            const data = NSData.alloc().initWithBase64EncodedStringOptions(tempData, 1);
            // write to the file
            file.writeSync(data);
            // open the file
            utils.ios.openFile(file.path);
            return { written: true, message: 'File written.' };
        } catch (e) {
            return { written: false, message: e.message };
        }
    }
}
