# Nativescript Create PDF

A plugin to help create and save a PDF file to the users device that includes text and images.

## Prerequisites / Requirements

Install the Nativescript-permissions plugin from: https://www.npmjs.com/package/nativescript-permissions

```javascript
npm i nativescript-permissions
```

Your PDF will not be written without permission to READ and WRITE to the filesystem.

## Installation

```javascript
tns plugin add @microexcel-csd/nativescript-create-pdf
```

## Usage 

Import the plugin into your component
	
```typescript
import { CreatePdf } from "@microexcel-csd/nativescript-create-pdf";
```

Init the plugin

```typescript
createPdf = new CreatePdf();
```

Request proper Android permissions:

```typescript
 ngOnInit(): void {
        permissions.requestPermission([
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE",
        ], "We need these permissions to save pdf")
        .then(function (res) {
        })
        .catch(function () {
        });
    }
```

Create your PDF file settings:

```typescript
fileSettings = {
        orientation: 'portrait',
        units: 'mm',
        paperSize: 'Letter',
        fileName: 'pdf-export.pdf', // Name your file with the extension
        folderName: 'App-Exports' // Name the folder being created where your pdf will be saved
    }
```

Add your text:

```typescript
text = [
    {
        data: 'Page 1 Sample Text', // this is what appears on the page
        xPos: 10, // x coordinate from top left
        yPos: 20, // y coordinate from top left
        fontSize: 18,
        pageNum: 1 // the page your text will appear on
    },
    {
        data: 'Page 2 Sample Text',
        xPos: 10,
        yPos: 20,
        fontSize: 14,
        pageNum: 2
    }
];
```

Change font color:

Add **fontColor** to your text object with your RGB color values in an array. If **fontColor** is not included the default color is black.

```typescript
fontColor: [8, 134, 255]
```

Add your images:

```typescript
images = [
    {
        path: './app/assets/image.jpg', // where your image is located
        type: 'JPEG', // img type
        xPos: 10, // x coordinate from top left
        yPos: 40, // y coordinate from top left
        width: 30, // width of your image in same unit size as your file settings (mm in this example)
        height: 30, // height of your image in same unit size as your file settings (mm in this example)
        alias: null,
        compression: 'FAST',
        pageNum: 1 // the page your image will appear on
    },
    {
        path: './app/assets/image.jpg',
        type: 'JPEG',
        xPos: 10,
        yPos: 40,
        width: 100,
        height: 125,
        alias: null,
        compression: 'FAST',
        pageNum: 2
    }
];
```

Pass data and create your pdf:

```typescript
this.createPdf.create(this.fileSettings, this.text, this.images);
// this will create your pdf and write it to the device
```

## Credits
https://www.npmjs.com/package/jspdf
https://rawgit.com/MrRio/jsPDF/master/docs/jsPDF.html
    
## ISC License

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

## Support Our Packages
[![paypal](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LM74WLHTJN8BA)
