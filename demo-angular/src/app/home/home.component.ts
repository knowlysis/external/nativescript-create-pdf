import { Component, OnInit } from "@angular/core";
import { CreatePdf } from "@microexcel-csd/nativescript-create-pdf";
import * as permissions from "nativescript-permissions";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {
    fileSettings = {
        orientation: 'portrait',
        units: 'mm',
        paperSize: 'Letter',
        fileName: 'pdf-export.pdf',
        folderName: 'App-Exports'
    }

    images = [
    {
        path: './app/assets/image.jpg',
        type: 'JPEG',
        xPos: 10,
        yPos: 40,
        width: 30,
        height: 30,
        alias: null,
        compression: 'FAST',
        pageNum: 1
    },
    {
        path: './app/assets/image.jpg',
        type: 'JPEG',
        xPos: 10,
        yPos: 40,
        width: 100,
        height: 125,
        alias: null,
        compression: 'FAST',
        pageNum: 2
    }
];

text = [
    {
        data: 'Page 1 Sample Text',
        xPos: 10,
        yPos: 20,
        fontSize: 18,
        pageNum: 1,
        fontColor: [8, 134, 255]
    },
    {
        data: 'Page 2 Sample Text',
        xPos: 10,
        yPos: 20,
        fontSize: 14,
        pageNum: 2
    }
];
createPdf = new CreatePdf();
    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        permissions.requestPermission([
            "android.permission.INTERNET",
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE",
        ], "We need these permissions to save our pdf")
        .then(function (res) {
        })
        .catch(function () {
        });
    }

    async makePdf () {
        try {

            let result = await this.createPdf.create(this.fileSettings, this.text, this.images);
            console.log(`This is the result output: ${result.message}`);
            if (!result.written) {
                alert(`'Error writing PDF! ${result.message}`);
            }
        } catch (e) {
            alert(`Error writing PDF ${e.message}`)
        }
    }
}
